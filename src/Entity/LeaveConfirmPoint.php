<?php

namespace Drupal\leave_confirm\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\leave_confirm\LeaveConfirmPointInterface;

/**
 * Defines the LeaveConfirmPoint entity.
 *
 * The 'rendered' tag for the List cache is necessary since leave_confirms
 * have to be rerendered once they are modified. Invalidating the render cache
 * ensures we always display the correct leave_confirm for every form.
 *
 * @ConfigEntityType(
 *   id = "leave_confirm_point",
 *   label = @Translation("LeaveConfirm Point"),
 *   handlers = {
 *     "list_builder" = "Drupal\leave_confirm\Entity\Controller\LeaveConfirmPointListBuilder",
 *     "form" = {
 *       "add" = "Drupal\leave_confirm\Form\LeaveConfirmPointForm",
 *       "edit" = "Drupal\leave_confirm\Form\LeaveConfirmPointForm",
 *       "disable" = "Drupal\leave_confirm\Form\LeaveConfirmPointDisableForm",
 *       "enable" = "Drupal\leave_confirm\Form\LeaveConfirmPointEnableForm",
 *       "delete" = "Drupal\leave_confirm\Form\LeaveConfirmPointDeleteForm"
 *     }
 *   },
 *   config_prefix = "leave_confirm_point",
 *   admin_permission = "administer leave confirm settings",
 *   list_cache_tags = {
 *    "rendered"
 *   },
 *   entity_keys = {
 *     "id" = "formId",
 *     "label" = "label",
 *     "status" = "status",
 *   },
 *   config_export = {
 *     "formId",
 *     "label",
 *     "uuid",
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/people/leave_confirm/leave_confirm-points/{leave_confirm_point}",
 *     "disable" = "/admin/config/people/leave_confirm/leave_confirm-points/{leave_confirm_point}/disable",
 *     "enable" = "/admin/config/people/leave_confirm/leave_confirm-points/{leave_confirm_point}/enable",
 *     "delete-form" = "/admin/config/people/leave_confirm/leave_confirm-points/{leave_confirm_point}/delete",
 *   }
 * )
 */
class LeaveConfirmPoint extends ConfigEntityBase implements LeaveConfirmPointInterface {
  /**
   * The leave_confirm label.
   *
   * @var string
   */
  protected $label;

  /**
   * The formid associated with the leave_confirm.
   *
   * @var string
   */
  public $formId;

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->formId;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return $this->formId;
  }

  /**
   * {@inheritdoc}
   */
  public function setFormId($form_id) {
    $this->formId = $form_id;
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function setLabel($label) {
    $this->label = $label;
  }

}
