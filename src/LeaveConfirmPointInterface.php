<?php

namespace Drupal\leave_confirm;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * LeaveConfirmPoint Point Entity Interface.
 *
 * @package Drupal\leave_confirm
 *
 * Provides an interface defining a LeaveConfirmPointPoint entity.
 */
interface LeaveConfirmPointInterface extends ConfigEntityInterface {

  /**
   * Getter for form machine ID property.
   *
   * @return string
   *   Machine name form id.
   */
  public function getFormId();

  /**
   * Setter for label property.
   *
   * @param string $form_id
   *   Form machine ID string.
   */
  public function setFormId($form_id);

  /**
   * Getter for label property.
   *
   * @return string
   *   Label string.
   */
  public function getLabel();

  /**
   * Setter for label property.
   *
   * @param string $label
   *   Label string.
   */
  public function setLabel($label);

}
