<?php

namespace Drupal\leave_confirm\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Builds the form to delete a LeaveConfirm Point.
 */
class LeaveConfirmPointEnableForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to enable the confirmation prompt?');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('This will activate the leave confirmation feature for this form.');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('leave_confirm_point.list');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Enable');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->enable();
    $this->entity->save();
    $this->messenger()->addMessage($this->t('LeaveConfirm point %label has been enabled.', ['%label' => $this->entity->label() ?? $this->entity->id()]));
    $form_state->setRedirect('leave_confirm_point.list');
  }

}
