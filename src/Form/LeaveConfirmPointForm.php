<?php

namespace Drupal\leave_confirm\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Entity Form to edit LEAVECONFIRM points.
 */
class LeaveConfirmPointForm extends EntityForm {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * LeaveConfirmPointForm constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Constructor.
   */
  public function __construct(RequestStack $request_stack) {
    $this->requestStack = $request_stack;
  }

  /**
   * Create LeaveConfirm Points.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   Event to create LeaveConfirm points.
   *
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static(
    $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\leave_confirm\LeaveConfirmPointInterface $leave_confirmPoint */
    $leave_confirm_point = $this->entity;

    // Support to set a default form_id through a query argument.
    $request = $this->requestStack->getCurrentRequest();
    if ($leave_confirm_point->isNew() && !$leave_confirm_point->id() && $request->query->has('form_id')) {
      $leave_confirm_point->set('formId', $request->query->get('form_id'));
      $leave_confirm_point->set('label', $request->query->get('form_id'));
    }

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Form ID'),
      '#description' => $this->t('Also works with the base form ID.'),
      '#default_value' => $leave_confirm_point->label(),
      '#required' => TRUE,
    ];
    $form['details'] = [
      '#type' => 'details',
      '#title' => $this->t('Form ID Naming Handbook'),
    // Set to TRUE if you want the details fieldset to be open by default.
      '#open' => FALSE,
    ];

    // Add fields inside the details fieldset.
    $form['details']['additional_field'] = [
      '#markup' => "<p>The form ID for a node bundle will follow the pattern <code><b>node_[node_type]_form</b></code>, and for editing a node of a particular type, it will be <code><b>node_[node_type]_edit_form</b></code>. For example, the form ID for adding a 'page' node type will be <code><b>node_page_form</b></code>, and for editing a page node, it will be <code><b>node_page_edit_form</b></code>.</p>
			<p>For Webform, if the Webform&#39;s ID is &#39;contact&#39;, then the form ID will be <code><b>webform_submission_contact_form</b></code>. If the Webform is placed on a page using a block, the form ID will be <code><b>webform_submission_contact_form_add</b></code>.</p>
			<p>For user-related forms, the form IDs are as follows:</p>
			<ul>
			<li>Forgot password form: <code><b>user_pass</b></code>.</li>
			<li>Login form: <code><b>user_login_form</b></code>.</li>
			<li>User register form: <code><b>user_register_form</b></code>.</li>
			</ul>
			<p>For the Contact module&#39;s personal form, the form ID is <code><b>contact_message_personal_form</b></code>.</p>
			",
    ];

    $form['formId'] = [
      '#type' => 'machine_name',
      '#default_value' => $leave_confirm_point->id(),
      '#machine_name' => [
        'exists' => 'leave_confirm_point_load',
      ],
      '#disable' => !$leave_confirm_point->isNew(),
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var LeaveConfirmPoint $leave_confirm_point */
    $leave_confirm_point = $this->entity;
    $status = $leave_confirm_point->save();

    if ($status == SAVED_NEW) {
      $this->messenger()->addMessage($this->t('LeaveConfirm Point for %form_id form was created.', [
        '%form_id' => $leave_confirm_point->getFormId(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('LeaveConfirm Point for %form_id form was updated.', [
        '%form_id' => $leave_confirm_point->getFormId(),
      ]));
    }
    $form_state->setRedirect('leave_confirm_point.list');
    return $status;
  }

}
