<?php

namespace Drupal\leave_confirm\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Builds the form to delete a LeaveConfirm Point.
 */
class LeaveConfirmPointDisableForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to disable the LeaveConfirm?');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('This action will deactivate the leave confirmation functionality for this form.');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('leave_confirm_point.list');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Disable');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->disable();
    $this->entity->save();
    $this->messenger()->addMessage($this->t('LeaveConfirm point %label has been disabled.', ['%label' => $this->entity->label() ?? $this->entity->id()]));
    $form_state->setRedirect('leave_confirm_point.list');
  }

}
