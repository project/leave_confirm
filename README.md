The Leave Confirm module enhances the user experience on Drupal websites
by providing a configurable popup confirmation dialog when users attempt
to leave a page without saving changes in specified forms. This module
ensures that important form data is not accidentally lost and allows
users to confirm their intention before navigating away.

**Key Features:**

1.  **Configurable Leave Confirm Dialogs:** Admins can easily configure
the Leave Confirm feature for specific forms across the site. This
ensures that critical information entered in forms is protected
against accidental navigation without saving.
2.  **Granular Permissions:** Fine-tune access to the Leave Confirm
settings based on user roles. This allows administrators to control
which roles can configure the Leave Confirm feature for specific forms.
3.  **Default Configuration:** On module installation, the Leave Confirm
module automatically adds points for common forms, including user
forms, node forms, and webform forms. These points are pre-configured
to provide a seamless experience out of the box.
4.  **Enable/Disable Functionality:** Admins have the flexibility to
enable or disable the Leave Confirm feature for each configured form
point as per site requirements.
5.  **Add New Form Points:** In addition to the default forms,
administrators can effortlessly add new form points through the
module's configuration. This extensibility ensures that the Leave
Confirm feature can be applied to any form on the site.
6.  **User-Friendly Interface:** Manage leave confirmation settings
seamlessly through the administrative interface, accessible at the
route **admin/config/user-interface/leave-confirm-points**.

**How to Use:**

1.  Install and enable the Leave Confirm module.
2.  Configure Leave Confirm settings for default forms or add new form
points as needed. Access the settings at
**admin/config/user-interface/leave-confirm-points** to manage leave
confirmation for various forms.
3.  Fine-tune permissions for user roles to control access to the Leave
Confirm configuration.
4.  Enjoy an enhanced user experience with the added layer of
confirmation before leaving forms.

The Leave Confirm module is a valuable addition to any Drupal site,
providing an extra layer of data protection and user-friendly
interaction.
