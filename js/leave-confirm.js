(function ($, Drupal, drupalSettings) {
  'use strict';
  Drupal.behaviors.confirmLeave = {
    attach: function (context, settings) {

      if (drupalSettings.leave_confirm && drupalSettings.leave_confirm.formId) {
        // Access the form IDs.
        var formId = drupalSettings.leave_confirm.formId;

        $(formId).find('.form-item').on('formUpdated', function (e) {
          $('form').addClass('form-updated');

          window.onbeforeunload = function (e) {
            var dialogText = 'You have unsaved changes on this page. Are you sure you want to leave?';
            e.returnValue = dialogText;
            return dialogText;
          }

          $('form').on('submit', function (e) {
            window.onbeforeunload = NULL;
          });
        });
      }

    }
  };
}(jQuery, Drupal, drupalSettings));
